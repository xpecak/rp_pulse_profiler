/* Red Pitaya C API example Acquiring a signal from a buffer
 * This application acquires a signal on a specific channel */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include "rp.h"

#define BUFFERS_COUNT 20
#define BUFF_LENGTH 16384
#define MAX_TIME 3

//globals to make stuff easier
float* buffers[BUFFERS_COUNT] = {NULL};
uint32_t pulse_count = 0U;


bool allocate_buffers() {
    for (int i = 0; i < BUFFERS_COUNT; i++) {
        buffers[i] = malloc(BUFF_LENGTH * sizeof(float));
        if (buffers[i] == NULL) {
            printf("Allocating buffers failed!\n");
            return false;
        }
    }
    return true;
}

void free_buffers() {
    for (int i = 0; i < BUFFERS_COUNT; i++) {
        if (buffers[i] != NULL) {
            free(buffers[i]);
        }
    }
}

void save_data() {
    FILE* output_file = fopen("output.txt", "w");
    if (output_file == NULL) {
        printf("failed opening file!\n");
        return;
    }

    for (int pulse = 0; pulse < pulse_count; pulse++) {
        for (size_t i = 0; i < BUFF_LENGTH; i++) {
            if (i == BUFF_LENGTH - 1) {
                fprintf(output_file, "%f", buffers[pulse][i]);
            } else {
                fprintf(output_file, "%f,", buffers[pulse][i]);
            }
        }
        fprintf(output_file, "\n");
    }
    fflush(output_file);
    fclose(output_file);
    return;
}

int main(int argc, char **argv){

    printf("allocating buffers\n");
    if (!allocate_buffers()) {
        free_buffers();
        return 1;
    }

    if(rp_Init() != RP_OK){
        printf("Rp api init failed!\n");
        free_buffers();
        return 1;
    }

    rp_AcqReset();
    rp_AcqSetDecimation(RP_DEC_1);
    rp_AcqSetGain(RP_CH_1, RP_HIGH);
    rp_AcqSetTriggerDelay(7500);

    rp_AcqSetTriggerLevel(RP_CH_1, 0.5);
    rp_AcqSetTriggerHyst(0.4);

    uint32_t buffer_size = BUFF_LENGTH;
    uintmax_t current_time = time(NULL);
    uintmax_t start_time = current_time;
    uintmax_t stop_time = current_time + MAX_TIME;
    rp_acq_trig_state_t state = RP_TRIG_STATE_WAITING;
    
    
    rp_AcqStart();
    sleep(1);
    rp_AcqSetTriggerSrc(RP_TRIG_SRC_CHA_PE);

    while(current_time < stop_time && pulse_count < BUFFERS_COUNT) {
        rp_AcqGetTriggerState(&state);
        if (state == RP_TRIG_STATE_TRIGGERED) {
            state = RP_TRIG_STATE_WAITING;
            uint32_t trig_pos = 0U;
            rp_AcqGetWritePointerAtTrig(&trig_pos);
            //printf("trig %04u: %05u\n", pulse_count, trig_pos);
            rp_AcqGetDataV(RP_CH_1, trig_pos, &buffer_size, buffers[pulse_count]);
            pulse_count++;
            rp_AcqStart();
            rp_AcqSetTriggerSrc(RP_TRIG_SRC_CHA_PE);
        }
        current_time = time(NULL);
    }

    rp_Release();
    printf("%u pulses detected in %ju seconds\n", pulse_count, current_time - start_time);
    if (pulse_count == BUFFERS_COUNT) {
        printf("Buffers filled\n");
    } else {
        printf("Hit time limit\n");
    }
    printf("Saving data\n");
    save_data();
    printf("done\n");
    free_buffers();
    
    return 0;
}

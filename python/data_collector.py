import json
import click
import paramiko
import serial
import sys
import csv
from rich.console import Console
from rich.prompt import Confirm
from rich.progress import Progress, SpinnerColumn, TextColumn

sys.path.insert(0, "./cube_software")
from pyCubeLib.cube_comm import CubeComm

console = Console()

def validate_config(override, config_file):
    required_keys = ["output_file", "start_pos", "end_pos", "z_offset", "resolution", "count_time_ms", "average"]
    config_data = {}
    with open(config_file, "r") as fd:
        config_data = json.load(fd)
    if override:
        config_data.update(json.loads(override))

    missed = False
    for key in required_keys:
        if key not in config_data:
            console.log(f"[red]Config missing key : {key}")
            missed = True
    if missed:
        return None

    config_data.setdefault("experiment_name", "experiment1")
    return config_data


def connect_rp(rp_address):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    try:
        client.connect(rp_address, username="root")
    except Exception:
        raise

    try:
        rp_stdin, rp_stdout, rp_stderr = client.exec_command("/root/rp_server")
    except Exception:
        client.close()
        raise

    if rp_stderr.channel.recv_ready():
        rp_stdin.close()
        rp_stdout.close()
        rp_stderr.close()
        client.close()
        raise Exception("Failed to init RP hardware")

    return client, rp_stdin, rp_stdout, rp_stderr


def connect_cube(cube_path):
    try:
        serial_port = serial.Serial(cube_path, 115200, timeout = 0.1)
    except Exception:
        raise
    serial_port.flush()
    cube = CubeComm(100)
    cube.set_serial_port(serial_port)
    return serial_port, cube


def close_rp(client, rp_stdin, rp_stdout, rp_stderr):
    rp_stdin.write("0\n")
    console.log(f"server response to exit:{rp_stdout.readlines()}")
    rp_stdin.close()
    rp_stdout.close()
    rp_stderr.close()
    client.close()
    return


def home_cube(cube):
    console.log("Homing Cube")
    error, ret = cube.home()
    if error:
        console.log(f"[red]Comms error:{error}")
        return False
    if ret.error != 0:
        console.log(f"Cube error:{ret.error}")
        return False
    return True


def move_cube(cube, x, y, z):
    error, ret = cube.move_to(x, y, z)
    if error:
        console.log(f"[red]Comms error:{error}")
        return False
    if ret.error != 0:
        console.log(f"Cube error:{ret.error}")
        return False
    return True


def measure_count(rp_in, rp_out, rp_err, count_time, average):
    total = 0
    count = 0
    for i in range(average):
        rp_in.write(f"{count_time}\n")
        rp_in.flush()
        if rp_err.channel.recv_ready():
            console.log("[red]RP error:")
            console.log(rp_err.readlines())
            continue
        total += int(rp_out.readline()[2:-1])
        count += 1
    return total/count


def main_loop(cube, rp_stdin, rp_stdout, rp_stderr, config):
    if not home_cube(cube):
        console.log("[red]Homing failed")
        return

    output_file = open(config["output_file"], 'w', newline='')
    csv_out = csv.writer(output_file)
    csv_out.writerow(["x", "y", "count"]) #header

    x_steps = int((config["end_pos"][0] - config["start_pos"][0]) / config["resolution"])
    y_steps = int((config["end_pos"][1] - config["start_pos"][1]) / config["resolution"])
    total_steps = x_steps * y_steps

    console.log("Starting measurement")

    with Progress(
        SpinnerColumn(spinner_name="dots12"),
        *Progress.get_default_columns(),
        TextColumn("[green]pos: [blue bold]{task.fields[x]} {task.fields[y]}[/] [green]count: [blue bold]{task.fields[c]}"),
        transient=True,
        console=console
    ) as progress:
        task = progress.add_task("[blue] Measuring...", total = total_steps, x=0, y=0, c=0)
        progress.print()

        for y in range(y_steps):
            for x in range(x_steps):
                x_pos = config["start_pos"][0] + x * config["resolution"]
                y_pos = config["start_pos"][1] + y * config["resolution"]
                while not move_cube(cube, x_pos, y_pos, config["z_offset"]): continue
                count = measure_count(rp_stdin, rp_stdout, rp_stderr, config["count_time_ms"], config["average"])
                csv_out.writerow([x_pos, y_pos, count])
                progress.update(task, advance=1.0, x=x_pos, y=y_pos, c=count)


    console.log("Measurement finished")
    output_file.close()
    console.log("Output file closed")
    return



@click.command()
@click.option('--override', '-o', type=str, help="Override part of config json")
@click.option('--cube', '-c', 'cube_path', type=str, default='/dev/ttyACM0', help="Cube serial port")
@click.option('--redpitaya', '-rp', type=str, default='rp.local', help="Redpitaya address")
@click.option('--test', '-t', is_flag=True, help="Only test connections, print config info")
@click.argument('config_file', type=click.Path(exists=True, dir_okay=False), required=True)
def main(override, cube_path, redpitaya, test, config_file):
    console.log("Loading config files...")
    config = validate_config(override, config_file)
    if config is None:
        console.log("[red]Invalid config")
        return

    console.log(f"Connecting to Cube on {cube_path}")
    try:
        serial_port, cube = connect_cube(cube_path)
    except Exception as err:
        console.log(f"[red]Connecting to Cube failed:[/]\n{err}")
        exit(1)

    console.log(f"Connecting to RedPitaya @ {redpitaya}")
    try:
        rp_client, rp_stdin, rp_stdout, rp_stderr = connect_rp(redpitaya)
    except Exception as err:
        console.log(f"[red]Connecting to RedPitaya failed:[/]\n{err}")
        serial_port.close()
        exit(1)

    console.log("[blue]Loaded config:")
    console.log(config)

    Confirm.ask("[blue bold]Continue?", console=console, default=True)

    if test:
        console.log("Connection test finished, exiting")
        exit(0)

    main_loop(cube, rp_stdin, rp_stdout, rp_stderr, config)

    console.log("Closing SSH to RedPitaya")
    close_rp(rp_client, rp_stdin, rp_stdout, rp_stderr)
    console.log("Closing Cube serial")
    serial_port.close()

    return


if __name__ == '__main__':
    main()

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os
import sys
from skimage import restoration, filters
from helper_bits import loaddata, loadconfig


config_abs = os.path.abspath(sys.argv[1])
config = loadconfig(config_abs)
data = loaddata(config, os.path.dirname(config_abs))

area_x = config["end_pos"][0] - config["start_pos"][0]
area_y = config["end_pos"][1] - config["start_pos"][1]

print("res:", data.shape)
print("step:", config["resolution"])
print("time:", config["count_time_ms"] * config["average"])
print("area:", area_x, area_y)

data = data/np.amax(data)

data_denoised = restoration.denoise_tv_chambolle(data)
data_sharpened = filters.unsharp_mask(data_denoised, radius=3)

fig, axs = plt.subplots(nrows=1, ncols=3, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]
textsize = 'x-large'

axs[0].imshow(data, interpolation='none', cmap=colors)
axs[0].set_title("Original", fontsize=textsize)
axs[1].imshow(data_denoised, interpolation='none', cmap=colors)
axs[1].set_title("Denoising", fontsize=textsize)
axs[2].imshow(data_sharpened, interpolation='none', cmap=colors)
axs[2].set_title("Unsharp Masking", fontsize=textsize)

fig.tight_layout()
plt.show()
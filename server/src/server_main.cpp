#include <iostream>
#include <chrono>
#include "redpitaya.hpp"


void delay(std::chrono::milliseconds length) {
    auto start = std::chrono::steady_clock::now();
    auto stop = start + length;
    while(std::chrono::steady_clock::now() < stop) {
        continue;
    }
}

int main() {

    using namespace std::chrono_literals;

    rp::acq_parameters rp_config = {
        RP_DEC_1,
        RP_TRIG_SRC_CHA_NE,
        0,
        0.00,
        -0.03,
        -0.03,
        RP_LOW,
        RP_LOW
    };

    auto redpitaya = rp::manager(rp_config);

    if(!redpitaya.initialized) {
        std::cerr << "RedPitaya init failed, quiting!" << std::endl;
        return 1;
    }

    while(true) {
        uint32_t duration = 0;
        std::cin >> duration;
        if (duration == 0) {
            std::cout << "exit" << std::endl;
            return 0;
        }
        if (duration <= 10) {
            std::cerr << "duration too short" << std::endl;
            continue;
        }
        auto pulses = redpitaya.get_pulse_count(std::chrono::milliseconds(duration));
        std::cout << "p:" << pulses << std::endl;
    }

    return 0;
}

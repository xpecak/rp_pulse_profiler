#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

int main() {

	int listen_fd = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in serveraddr;
	bzero(&serveraddr, sizeof(serveraddr));

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htons(INADDR_ANY);
	serveraddr.sin_port = htons(2905);

	bind(listen_fd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	printf("Bound socket!\n");
	listen(listen_fd, 1);
	printf("Listening for connection...\n");
	int comm_fd = accept(listen_fd, NULL, NULL);
	printf("Client connected!\n");

	while(1) {
		char str[100];
		bzero(str, 100);
		ssize_t r = read(comm_fd, str, 100);
		if (r == 0) {
			printf("Broken connection!\n");
			break;		
		}
		printf("Received: %s\n", str);
		if (strcmp(str, "exit") == 0) {
			printf("Received exit!\n");
			break;
		}
	}
	close(comm_fd);
	close(listen_fd);
	
	return 0;
}

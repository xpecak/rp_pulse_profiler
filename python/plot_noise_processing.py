import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os
from functools import partial
from scipy import signal
from skimage import restoration
from helper_bits import loaddata, loadconfig


data_list = ("sim_data/noise_tests/uniform_grid_noise_high.json",
             "sim_data/noise_tests/hole_sizes_noise.json",
             "sim_data/noise_tests/spacing_grid_noise.json",
             "sim_data/noise_tests/spacing_lines_noise.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))

plt.figure(figsize=(20, 30))

gs = mpl.gridspec.GridSpec(8, 4, width_ratios=[1, 1, 1, 1],
         wspace=0.05, hspace=0.05, top=0.95, bottom=0.05, left=0.17, right=0.845)

colors = mpl.colormaps["viridis"]
f_size = "x-large"

ax=plt.subplot(gs[0, 0])
ax.imshow(data[0], interpolation='none', cmap=colors)
ax.set_title("Uniform grid", fontsize=f_size)
ax.set_ylabel("Original", fontsize=f_size)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax=plt.subplot(gs[0, 1])
ax.imshow(data[1], interpolation='none', cmap=colors)
ax.set_title("Hole sizes", fontsize=f_size)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax=plt.subplot(gs[0, 2])
ax.imshow(data[2], interpolation='none', cmap=colors)
ax.set_title("Spacing grid", fontsize=f_size)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax=plt.subplot(gs[0, 3])
ax.imshow(data[3], interpolation='none', cmap=colors)
ax.set_title("Spacing lines", fontsize=f_size)
ax.set_xticklabels([])
ax.set_yticklabels([])

methods = [
    ("Median", partial(signal.medfilt2d, kernel_size=3)),
    ("Wiener", partial(signal.wiener, mysize=[3,3])),
    ("Bilateral", partial(restoration.denoise_bilateral)),
    ("Non-Local Means", partial(restoration.denoise_nl_means, patch_size=4)),
    ("Total Variation\n(Bregman)", partial(restoration.denoise_tv_bregman)),
    ("Total Variation\n(Chambolle)", partial(restoration.denoise_tv_chambolle)),
    ("Wavelet", partial(restoration.denoise_wavelet))
]

row_indx = 1
for name, func in methods:
    for index, item in enumerate(data):
        normalized = item/np.amax(item)
        filtered = func(normalized)
        ax = plt.subplot(gs[row_indx, index])
        if (index == 0):
            ax.set_ylabel(name, fontsize=f_size)
        ax.imshow(filtered, interpolation='none', cmap=colors)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
    row_indx += 1

#plt.show()
plt.savefig("noise_processing.pdf", dpi=800, bbox_inches='tight')

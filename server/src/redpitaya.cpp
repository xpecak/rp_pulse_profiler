#include "redpitaya.hpp"

namespace rp {

manager::manager(acq_parameters& initial_config) {
    config = initial_config;

    initialized = (rp_Init() == RP_OK);

    if (initialized) {
        rp_AcqReset();
        rp_AcqSetDecimation(config.decimation);
        rp_AcqSetTriggerSrc(config.trigger_source);
        rp_AcqSetTriggerDelay(config.trigger_delay);
        rp_AcqSetTriggerHyst(config.hysterisis);
        rp_AcqSetGain(RP_CH_1, config.ch1_gain);
        rp_AcqSetGain(RP_CH_2, config.ch2_gain);
        rp_AcqSetTriggerLevel(RP_T_CH_1, config.ch1_trig_level);
        rp_AcqSetTriggerLevel(RP_T_CH_2, config.ch2_trig_level);
    }
}

manager::~manager() {
    rp_Release();
}

bool manager::set_parameters(acq_parameters& new_config) {
    config = new_config;
    rp_AcqReset();
    rp_AcqSetDecimation(config.decimation);
    rp_AcqSetTriggerSrc(config.trigger_source);
    rp_AcqSetTriggerDelay(config.trigger_delay);
    rp_AcqSetTriggerHyst(config.hysterisis);
    rp_AcqSetGain(RP_CH_1, config.ch1_gain);
    rp_AcqSetGain(RP_CH_2, config.ch2_gain);
    rp_AcqSetTriggerLevel(RP_T_CH_1, config.ch1_trig_level);
    rp_AcqSetTriggerLevel(RP_T_CH_2, config.ch2_trig_level);
    return true;
}

acq_parameters manager::get_parameters() {
    return config;
}

uint32_t manager::get_pulse_count(std::chrono::milliseconds duration) {
    uint32_t pulse_count = 0;
    rp_acq_trig_state_t state = RP_TRIG_STATE_WAITING;
    auto stop = std::chrono::steady_clock::now() + duration;

    while(std::chrono::steady_clock::now() < stop) {
        rp_AcqGetTriggerState(&state);
        if (state == RP_TRIG_STATE_TRIGGERED) {
            pulse_count++;
            state = RP_TRIG_STATE_WAITING;
            rp_AcqStart();
            rp_AcqSetTriggerSrc(config.trigger_source);
        }
    }

    return pulse_count;
}

}

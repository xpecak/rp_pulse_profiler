#pragma once
#include "rp.h"
#include <chrono>

namespace rp {


struct acq_parameters {
    rp_acq_decimation_t decimation;
    rp_acq_trig_src_t trigger_source;
    int32_t trigger_delay;
    float hysterisis;
    float ch1_trig_level;
    float ch2_trig_level;
    rp_pinState_t ch1_gain;
    rp_pinState_t ch2_gain;
};


class manager {

acq_parameters config;

public:

bool initialized = false;

manager(acq_parameters& initial_config);

~manager();

bool set_parameters(acq_parameters& params);

acq_parameters get_parameters();

uint32_t get_pulse_count(std::chrono::milliseconds duration);

};

} //namespace rp
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include "rp.h"


int main() {

    if(rp_Init() != RP_OK){
        printf("Rp api init failed!\n");
        return 1;
    }
    
    rp_AcqReset();
    rp_AcqSetDecimation(RP_DEC_1);
    rp_AcqSetGain(RP_CH_1, RP_HIGH);
    rp_AcqSetTriggerDelay(0);
    rp_AcqSetTriggerLevel(RP_CH_1, -0.2);
    rp_AcqStart();
    rp_AcqSetTriggerSrc(RP_TRIG_SRC_CHA_NE);

    while(1) {
        uintmax_t current_time = time(NULL);
        uintmax_t stop_time = current_time + 1;
        uint32_t pulse_count = 0;
        rp_acq_trig_state_t state = RP_TRIG_STATE_WAITING;

        while(current_time < stop_time) {
            rp_AcqGetTriggerState(&state);
            if (state == RP_TRIG_STATE_TRIGGERED) {
                pulse_count++;
                state = RP_TRIG_STATE_WAITING;
                rp_AcqStart();
                rp_AcqSetTriggerSrc(RP_TRIG_SRC_CHA_PE);
            }
            current_time = time(NULL);
        }
        printf("Detecting %6u pulses / second\r", pulse_count);
        fflush(stdout);
    }
}

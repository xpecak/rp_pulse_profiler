import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import sys
import os
from scipy import signal
from skimage import restoration
from helper_bits import loaddata, loadconfig


config_abs = os.path.abspath(sys.argv[1])

config = loadconfig(config_abs)
data = loaddata(config, os.path.dirname(config_abs))
max_value = np.amax(data)

data = data/max_value

filtered = signal.wiener(data, [3,3])
filtered2 = signal.medfilt2d(data, 3)
filtered3 = restoration.denoise_nl_means(data, patch_size=4)
filtered4 = restoration.denoise_bilateral(data)
filtered5 = restoration.denoise_tv_bregman(data)
filtered6 = restoration.denoise_tv_chambolle(data)
filtered7 = restoration.denoise_wavelet(data)

fig, axs = plt.subplots(2, 4, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

axs[0, 0].imshow(data, interpolation='none', cmap=colors)
axs[0, 0].set_title("Original")
axs[0, 1].imshow(filtered, interpolation='none', cmap=colors)
axs[0, 1].set_title("Wiener filter")
axs[0, 2].imshow(filtered2, interpolation='none', cmap=colors)
axs[0, 2].set_title("Median")
axs[0, 3].imshow(filtered3, interpolation='none', cmap=colors)
axs[0, 3].set_title("Non-local means")
axs[1, 0].imshow(filtered4, interpolation='none', cmap=colors)
axs[1, 0].set_title("Bilateral")
axs[1, 1].imshow(filtered5, interpolation='none', cmap=colors)
axs[1, 1].set_title("TV Bregman")
axs[1, 2].imshow(filtered6, interpolation='none', cmap=colors)
axs[1, 2].set_title("TV Chambolle")
axs[1, 3].imshow(filtered7, interpolation='none', cmap=colors)
axs[1, 3].set_title("Wavelet")
plt.show()

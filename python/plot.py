import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt("output.txt", delimiter=",")
data = np.roll(data, 691, axis=1)

count = np.shape(data)[0]

alpha_val = 0.1

average_data = []

for x in range(16384):
    average = 0
    for i in range(count):
        average += data[i][x]
    average /= count
    average_data.append(average)

for i in range(count):
    plt.plot(data[i], alpha=alpha_val, color='b')

plt.plot(average_data, alpha=0.5, color='r')

plt.axvline(x=691, color='k')

plt.show()

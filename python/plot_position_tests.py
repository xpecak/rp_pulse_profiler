import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from helper_bits import loaddata, loadconfig


data_list = ("sim_data/position_tests/hole_sizes_near.json",
             "sim_data/position_tests/spacing_grid_near.json",
             "sim_data/position_tests/spacing_lines_near.json",
             "sim_data/position_tests/hole_sizes_mid.json",
             "sim_data/position_tests/spacing_grid_mid.json",
             "sim_data/position_tests/spacing_lines_mid.json",
             "sim_data/position_tests/hole_sizes_far.json",
             "sim_data/position_tests/spacing_grid_far.json",
             "sim_data/position_tests/spacing_lines_far.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))


fig, axs = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

axs[0, 0].imshow(data[0], interpolation='none', cmap=colors)
axs[0, 0].set_title("Hole sizes", fontsize='x-large')
axs[0, 0].set_ylabel("5 mm", fontsize='x-large')
axs[0, 1].imshow(data[1], interpolation='none', cmap=colors)
axs[0, 1].set_title("Spacing grid", fontsize='x-large')
axs[0, 2].imshow(data[2], interpolation='none', cmap=colors)
axs[0, 2].set_title("Spacing lines", fontsize='x-large')
axs[1, 0].imshow(data[3], interpolation='none', cmap=colors)
axs[1, 0].set_ylabel("25 mm", fontsize='x-large')
axs[1, 1].imshow(data[4], interpolation='none', cmap=colors)
axs[1, 2].imshow(data[5], interpolation='none', cmap=colors)
axs[2, 0].imshow(data[6], interpolation='none', cmap=colors)
axs[2, 0].set_ylabel("60 mm", fontsize='x-large')
axs[2, 1].imshow(data[7], interpolation='none', cmap=colors)
axs[2, 2].imshow(data[8], interpolation='none', cmap=colors)


fig.tight_layout()
plt.show()

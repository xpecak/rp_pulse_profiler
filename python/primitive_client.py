#basic server for showing continuous pulse count
#should get deprecated quite quickly

import paramiko
import sys

RP_ADDRESS = "rp-f05735.local"
APP_COMMAND = "/root/rp_pulse_profiler/server/build/redpitaya_server"
COUNT_TIME_MS = int(sys.argv[1])

client = paramiko.SSHClient()
client.load_system_host_keys()

try:
    client.connect(RP_ADDRESS, username="root")
except Exception:
    print("Failed to connect!")
    exit(1)

print("Connected to RedPitaya")

try:
    ssh_stdin, ssh_stdout, ssh_stderr = client.exec_command(APP_COMMAND)
except Exception:
    print("Failed to launch server!")
    client.close()
    exit(1)

print("Launched server")

if ssh_stderr.channel.recv_ready():
    print("Failed to init RP hw!")
    ssh_stdout.close()
    ssh_stdin.close()
    ssh_stderr.close()
    client.close()

print(f"Count time: {COUNT_TIME_MS}ms")

while True:
    try:
        ssh_stdin.write(f"{COUNT_TIME_MS}\n")
        ssh_stdin.flush()
        data = ssh_stdout.readline()
        print(data[:-1], end="         \r")
    except KeyboardInterrupt:
        break

print("shutting down remote server")
ssh_stdin.write("0\n")
print(f"server:{ssh_stdout.readlines()}")
print("closing channels and connection")
ssh_stdout.close()
ssh_stdin.close()
ssh_stderr.close()
client.close()

import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from helper_bits import loaddata, loadconfig


data_list = ("sim_data/noise_tests/uniform_grid_noise_low.json",
             "sim_data/noise_tests/uniform_grid_noise_medium.json",
             "sim_data/noise_tests/uniform_grid_noise_high.json",
             "sim_data/noise_tests/hole_sizes_noise.json",
             "sim_data/noise_tests/spacing_grid_noise.json",
             "sim_data/noise_tests/spacing_lines_noise.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))

fig, axs = plt.subplots(nrows=2, ncols=3, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

axs[0, 0].imshow(data[0], interpolation='none', cmap=colors)
axs[0, 0].set_title("Uniform grid 'low'", fontsize='x-large')
axs[0, 1].imshow(data[1], interpolation='none', cmap=colors)
axs[0, 1].set_title("Uniform grid 'medium'", fontsize='x-large')
axs[0, 2].imshow(data[2], interpolation='none', cmap=colors)
axs[0, 2].set_title("Uniform grid 'high'", fontsize='x-large')
axs[1, 0].imshow(data[3], interpolation='none', cmap=colors)
axs[1, 0].set_title("Hole sizes 'high'", fontsize='x-large')
axs[1, 1].imshow(data[4], interpolation='none', cmap=colors)
axs[1, 1].set_title("Spacing grid 'high'", fontsize='x-large')
axs[1, 2].imshow(data[5], interpolation='none', cmap=colors)
axs[1, 2].set_title("Spacing lines 'high'", fontsize='x-large')

fig.tight_layout()
plt.show()

import matplotlib.pyplot as plt
import numpy as np
import sys
import os
from skimage import restoration, filters

from helper_bits import loadconfig, loaddata

def generate_psf(size, radius, resolution):
    data = np.empty([size, size])
    for x in range(size):
        for y in range(size):
            x_pos = (x - size/2) * resolution + resolution/2
            y_pos = (y - size/2) * resolution + resolution/2
            if (x_pos * x_pos + y_pos * y_pos) <= radius * radius:
                data[x, y] = 1.0
            else:
                data[x, y] = 0.0
    return data


config_abs = os.path.abspath(sys.argv[1])

config = loadconfig(config_abs)
data = loaddata(config, os.path.dirname(config_abs))
max_value = np.amax(data)

data = data/max_value

filtered = restoration.denoise_tv_chambolle(data)
unsharp = filters.unsharp_mask(data, radius=4, amount=1.0)

filun = filters.unsharp_mask(filtered, radius=4, amount=1.0)
unfil = restoration.denoise_tv_chambolle(unsharp)

fig, axs = plt.subplots(1, 4)

psf = generate_psf(49, 50/65, config["resolution"])

rc_deconv = restoration.richardson_lucy(data, psf, 10, clip=False)
wiener_deconv = restoration.wiener(data, psf, 2000)

axs[0].imshow(data, interpolation='none')
axs[0].set_title("Original")
axs[1].imshow(psf, interpolation='none')
axs[1].set_title("PSF")
axs[2].imshow(rc_deconv, interpolation='none')
axs[2].set_title("Richardson-Lucy")
axs[3].imshow(wiener_deconv, interpolation='none')
axs[3].set_title("Wiener")
plt.show()

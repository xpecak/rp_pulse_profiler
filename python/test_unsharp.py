import matplotlib.pyplot as plt
import numpy as np
import sys
import os
from skimage import restoration, filters
from helper_bits import loadconfig, loaddata


config_abs = os.path.abspath(sys.argv[1])

config = loadconfig(config_abs)
data = loaddata(config, os.path.dirname(config_abs))
max_value = np.amax(data)

data = data/max_value

filtered = restoration.denoise_tv_chambolle(data)
unsharp = filters.unsharp_mask(data, radius=4, amount=1.0)

filun = filters.unsharp_mask(filtered, radius=4, amount=1.0)
unfil = restoration.denoise_tv_chambolle(unsharp)

fig, axs = plt.subplots(1, 5, sharex=True, sharey=True)

axs[0].imshow(data, interpolation='none')
axs[0].set_title("Original")
axs[1].imshow(filtered, interpolation='none')
axs[1].set_title("Denoise")
axs[2].imshow(unsharp, interpolation='none')
axs[2].set_title("Unsharp")
axs[3].imshow(filun, interpolation='none')
axs[3].set_title("Denoise + Unsharp")
axs[4].imshow(unfil, interpolation='none')
axs[4].set_title("Unsharp + Denoise")
plt.show()

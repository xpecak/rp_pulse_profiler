import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from helper_bits import loaddata, loadconfig


data_list = ("sim_data/basic_tests/hole_sizes.json",
             "sim_data/basic_tests/uniform_grid.json",
             "sim_data/basic_tests/spacing_grid.json",
             "sim_data/basic_tests/spacing_lines.json",
             "sim_data/basic_tests/hole_sizes_sensor_shift.json",
             "sim_data/basic_tests/uniform_grid_sensor_shift.json",
             "sim_data/basic_tests/spacing_grid_sensor_shift.json",
             "sim_data/basic_tests/spacing_lines_sensor_shift.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))


fig, axs = plt.subplots(nrows=2, ncols=4, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

axs[0, 0].imshow(data[0], interpolation='none', cmap=colors)
axs[0, 0].set_title("Hole sizes", fontsize='x-large')
axs[0, 0].set_ylabel("Target shift", fontsize='x-large')
axs[0, 1].imshow(data[1], interpolation='none', cmap=colors)
axs[0, 1].set_title("Uniform grid", fontsize='x-large')
axs[0, 2].imshow(data[2], interpolation='none', cmap=colors)
axs[0, 2].set_title("Spacing grid", fontsize='x-large')
axs[0, 3].imshow(data[3], interpolation='none', cmap=colors)
axs[0, 3].set_title("Spacing lines", fontsize='x-large')
axs[1, 0].imshow(data[4], interpolation='none', cmap=colors)
axs[1, 0].set_ylabel("Sensor shift", fontsize='x-large')
axs[1, 1].imshow(data[5], interpolation='none', cmap=colors)
axs[1, 2].imshow(data[6], interpolation='none', cmap=colors)
axs[1, 3].imshow(data[7], interpolation='none', cmap=colors)


fig.tight_layout()
plt.show()

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import csv
import json
import sys
import os

def loadconfig(filename):
    print("config location:", filename)
    with open(filename) as config_file:
        config = json.load(config_file)
    return config

def loaddata(config, path):

    start = config["start_pos"]
    stop = config["end_pos"]
    resolution = config["resolution"]

    size_x = int((stop[0] - start[0])/resolution)
    size_y = int((stop[1] - start[1])/resolution)

    data = np.empty([size_x, size_y])

    input_file_path = os.path.join(path, config["output_file"])
    print("data file location:", input_file_path)

    with open(input_file_path, newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            x = int((float(row[0]) - start[0]) / resolution)
            y = int((float(row[1]) - start[1]) / resolution)
            count = float(row[2])
            data[x, y] = count
    return data



data_count = len(sys.argv[1:])
fig, axs = plt.subplots(1, data_count)

for index, arg in enumerate(sys.argv[1:]):
    config_abs = os.path.abspath(arg)
    config = loadconfig(config_abs)
    data = loaddata(config, os.path.dirname(config_abs))
    if (data_count == 1):
        axs.set_title(config["experiment_name"])
        axs.imshow(data, interpolation='none')
        continue
    axs[index].set_title(config["experiment_name"])
    axs[index].imshow(data, interpolation='none')

plt.show()

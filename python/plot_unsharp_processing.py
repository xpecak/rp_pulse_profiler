import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os
from helper_bits import loaddata, loadconfig
from skimage import restoration, filters


data_list = ("sim_data/noise_tests/uniform_grid_noise_high.json",
             "sim_data/noise_tests/hole_sizes_noise.json",
             "sim_data/noise_tests/spacing_grid_noise.json",
             "sim_data/noise_tests/spacing_lines_noise.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))

fig, axs = plt.subplots(nrows=4, ncols=4, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

data_normalized = [item/np.amax(item) for item in data]

data_filtered = [restoration.denoise_tv_chambolle(item) for item in data_normalized]

unsharp_10 = [filters.unsharp_mask(item, radius=10) for item in data_filtered]
unsharp_4 = [filters.unsharp_mask(item, radius=3) for item in data_filtered]

axs[0, 0].set_ylabel("Original", fontsize='x-large')
axs[0, 0].imshow(data[0], interpolation='none', cmap=colors)
axs[0, 1].imshow(data[1], interpolation='none', cmap=colors)
axs[0, 2].imshow(data[2], interpolation='none', cmap=colors)
axs[0, 3].imshow(data[3], interpolation='none', cmap=colors)

axs[1, 0].set_ylabel("Denoised", fontsize='x-large')
axs[1, 0].imshow(data_filtered[0], interpolation='none', cmap=colors)
axs[1, 1].imshow(data_filtered[1], interpolation='none', cmap=colors)
axs[1, 2].imshow(data_filtered[2], interpolation='none', cmap=colors)
axs[1, 3].imshow(data_filtered[3], interpolation='none', cmap=colors)

axs[2, 0].set_ylabel("Unsharp mask\n(radius=10)", fontsize='x-large')
axs[2, 0].imshow(unsharp_10[0], interpolation='none', cmap=colors)
axs[2, 1].imshow(unsharp_10[1], interpolation='none', cmap=colors)
axs[2, 2].imshow(unsharp_10[2], interpolation='none', cmap=colors)
axs[2, 3].imshow(unsharp_10[3], interpolation='none', cmap=colors)

axs[3, 0].set_ylabel("Unsharp mask\n(radius=3)", fontsize='x-large')
axs[3, 0].imshow(unsharp_4[0], interpolation='none', cmap=colors)
axs[3, 1].imshow(unsharp_4[1], interpolation='none', cmap=colors)
axs[3, 2].imshow(unsharp_4[2], interpolation='none', cmap=colors)
axs[3, 3].imshow(unsharp_4[3], interpolation='none', cmap=colors)


fig.tight_layout()
plt.show()

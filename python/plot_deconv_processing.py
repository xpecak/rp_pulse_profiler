import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os
from helper_bits import loaddata, loadconfig
from skimage import restoration, filters

def generate_psf(size, radius, resolution):
    data = np.empty([size, size])
    for x in range(size):
        for y in range(size):
            x_pos = (x - size/2) * resolution + resolution/2
            y_pos = (y - size/2) * resolution + resolution/2
            if (x_pos * x_pos + y_pos * y_pos) <= radius * radius:
                data[x, y] = 1.0
            else:
                data[x, y] = 0.0
    return data


data_list = ("sim_data/noise_tests/uniform_grid_noise_high.json",
             "sim_data/noise_tests/hole_sizes_noise.json",
             "sim_data/noise_tests/spacing_grid_noise.json",
             "sim_data/noise_tests/spacing_lines_noise.json"
             )
data = []

for item in data_list:
    config_abs = os.path.abspath(item)
    config = loadconfig(config_abs)
    data.append(loaddata(config, os.path.dirname(config_abs)))

fig, axs = plt.subplots(nrows=3, ncols=4, sharex=True, sharey=True)

colors = mpl.colormaps["viridis"]

data_normalized = [item/np.amax(item) for item in data]

psf = generate_psf(49, 50/65, 0.25)

data_wiener = [restoration.wiener(item, psf, 5000) for item in data_normalized]
data_rl = [restoration.richardson_lucy(item, psf, 10) for item in data_normalized]

axs[0, 0].set_ylabel("Original", fontsize='x-large')
axs[0, 0].imshow(data[0], interpolation='none', cmap=colors)
axs[0, 1].imshow(data[1], interpolation='none', cmap=colors)
axs[0, 2].imshow(data[2], interpolation='none', cmap=colors)
axs[0, 3].imshow(data[3], interpolation='none', cmap=colors)

axs[1, 0].set_ylabel("Wiener", fontsize='x-large')
axs[1, 0].imshow(data_wiener[0], interpolation='none', cmap=colors)
axs[1, 1].imshow(data_wiener[1], interpolation='none', cmap=colors)
axs[1, 2].imshow(data_wiener[2], interpolation='none', cmap=colors)
axs[1, 3].imshow(data_wiener[3], interpolation='none', cmap=colors)

axs[2, 0].set_ylabel("Richardson-Lucy\n(10 iterations)", fontsize='x-large')
axs[2, 0].imshow(data_rl[0], interpolation='none', cmap=colors)
axs[2, 1].imshow(data_rl[1], interpolation='none', cmap=colors)
axs[2, 2].imshow(data_rl[2], interpolation='none', cmap=colors)
axs[2, 3].imshow(data_rl[3], interpolation='none', cmap=colors)


fig.tight_layout()
plt.show()

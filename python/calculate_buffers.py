from rich.console import Console
from rich.table import Table
import decimal
from math import floor

decimal.getcontext().prec = 16
decimal.getcontext().clamp = 1

BUFFER_LENGTH = decimal.Decimal(16384)
SAMPLING_RATE = decimal.Decimal(125000000)

PREFIXES = {
    -9: "n",
    -6: "u",
    -3: "m",
    0: " ",
    3: "k",
    6: "M",
    9: "G"
}

def normalize(number):
    shift_amount = number.adjusted()
    base_3 = floor(shift_amount/3) * 3
    return str(number.scaleb(-base_3).normalize()) + " " + PREFIXES[base_3]

table = Table(title="STEMLab125 buffer calculations")

table.add_column("Decimation", justify="right")
table.add_column("Sampling rate", justify="right")
table.add_column("Buffer step", justify="right")
table.add_column("Buffer length", justify="right")

decimation = decimal.Decimal(1)

while(decimation < 65537):
    actual_sample_rate = SAMPLING_RATE / decimation
    buffer_step = decimal.Decimal(1) / actual_sample_rate
    buffer_duration = buffer_step * BUFFER_LENGTH
    table.add_row(f"{decimation}", f"{normalize(actual_sample_rate)}Hz", f"{normalize(buffer_step)}s", f"{normalize(buffer_duration)}s")
    decimation *= 2

console = Console(record=True)
console.print(table)
